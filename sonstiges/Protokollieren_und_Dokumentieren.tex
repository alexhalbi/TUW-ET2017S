% Hinweise für Protokollierung "Elektrotechnik für Informatik" 

\NeedsTeXFormat{LaTeX2e}
\documentclass[12pt,a4paper]{article}

\usepackage[ngerman]{babel} % german text
\usepackage[DIV12]{typearea} % size of printable area
\usepackage[T1]{fontenc} % font encoding
\usepackage[utf8x]{inputenc} % probably on Linux

\usepackage{graphicx} % to include images
\usepackage{subfigure} % for creating subfigures
\usepackage{amsmath} % a bunch of symbols
\usepackage{amssymb} % even more symbols
\usepackage{booktabs} % pretty tables

% a floating environment for circuits
\usepackage{float}
\usepackage{caption}

\newfloat{circuit}{tbph}{circuits}
\floatname{circuit}{Schaltplan}

% a floating environment for diagrams
\newfloat{diagram}{tbph}{diagrams}
\floatname{diagram}{Diagramm}

% \renewcommand{\familydefault}{\sfdefault} % activate to use sans-serif font as default

\sloppy % friendly typesetting

\usepackage{eurosym}
\usepackage{makeidx}
\usepackage{amsfonts}
\usepackage{mparhack}
\usepackage{array}
\usepackage{tabularx}
\usepackage{minitoc}
\usepackage[colorlinks=true]{hyperref}
\usepackage{epstopdf}
\usepackage{setspace}
\usepackage{csquotes}

\begin{document}
	
	\begin{titlepage}
		
		\begin{figure*}[!ht]
			\includegraphics[width=8cm]{../TULogo_CMYK}
		\end{figure*}
		
		\begin{center}
			\vspace*{1.3cm}
			{\large Elektrotechnische Grundlagen\\(LU 182.692)\\}
			\vspace{1.7cm}

			{\LARGE Hinweise zum Protokollieren und Dokumentieren von technischen 
			oder wissenschaftlichen Arbeiten \par}

			\vspace{2.7cm}

			{Christian Doiber\\
			 Andreas Fenk\\
			 Wolfgang Tomischko\\
			 Anton Zellhofer\\
			 Günter Fafilek\\
			 }
			\vfill

			
			{Technische Universität Wien\\
				Institut für Technische Informatik (182)\\
				1040, Treitlstraße 3, 3.Stock\\
				\today}
			
		\end{center}
	\end{titlepage}

\thispagestyle{empty}
\tableofcontents
% \listoffigures
\newpage
\setcounter{page}{1}

\section*{Vorwort}

Die hier beschriebenen HowTo's beziehen sich zwar auf die Laborübungen Elektrotechnische Grundlagen (LU 182.692), werden ihnen aber auch in anderen Bereichen helfen Ergebnisse und Erkenntnisse von Experimenten, Untersuchungen oder einfach nur theoretischen Überlegungen strukturiert für eine spätere Verwendung aufzuzeichnen. Selbstverständlich sind dies keine absolut gültigen Regeln und es ist genug Spielraum eine individuelle Methodik zur Dokumentation zu entwickeln. Manchesmal jedoch, z.B. in manchen Unternehmen oder Institutionen, ist die technisch/wissenschaftliche Dokumentation stark formalisiert und standardisiert, in manchen Fällen sogar genormt (z.B. im Rahmen von Qualitätsmanagement, Stichwort ISO 9000). In diesen Fällen bleibt kein Spielraum für Individualismus.\\

Wir wünschen gutes Gelingen beim Schreiben der Protokolle!
\newpage
	

\section{Generelle Richtlinien}

Jede(r) Student(in) sollte sich eine Mappe mit den Versuchsprotokollen
anlegen. Welche(r) Student(in) das Laborprotokoll erstellt liegt im
Ermessen der Laborgruppe. Es wäre jedoch von Vorteil, wenn
entsprechende Kopien für die Teilnehmer der Gruppe angefertigt
werden. \emph{Jede(r)} in der Gruppe muss mit dem Inhalt des
Protokolls vertraut sein. Das Protokoll selbst ist in jedem Fall ein
wesentlicher Bestandteil zur Beurteilung dieser Übung. Denken sie daran, dass 
alle Mitglieder einer Gruppe die gleiche Beurteilung für das Protokoll erhalten.
Zwingend vorgeschrieben ist die Verwendung des entsprechend zur Laborübung
passenden Deckblattes. Dies erleichtert uns die Zuordnung der Teilnehmer und Gruppen zu 
den einzelnen Übungsteilen und deren Bewertung. Da sie StudentInnen der Informatik sind, werden sie gebeten die Ausarbeitung der Laborprotokolle generell mit Unterstützung diverser Text-, Grafik- und
Tabellenkalkulationsprogramme durchzuführen. Falls sie mit \LaTeX{}
arbeiten wollen, können sie den Quellcode dieses Dokuments als Vorlage
verwenden.

	\begin{figure}[!h]
		\centering
		\includegraphics[width=0.6\textwidth]{bilder/Deckblatt1.pdf}
		\caption{Deckblatt für die erste Übung}
		\label{Deckblatt}
	\end{figure}


\section{Richtlinien zur Protokollführung}

Die am Versuchsplatz gewonnenen Ergebnisse sind \emph{während} der
Laborübung zu dokumentieren und schriftlich in Form eines gedruckten
Protokolls bis zur nächsten Laborübung mitzubringen.\\ 
Reproduzierbarkeit ist ein wesentliches Element wissenschaftlichen und technischen Fortschritts. Um einen Versuch erfolgreich wiederholen zu können, d.h. das gleiche Ergebnis zu reproduzieren, müssen alle wesentlichen Umstände und Parameter des Versuches festgelegt sein. Dies gelingt, bei komplizierteren Experimenten, nur durch genaue Beobachtung und Dokumentation der Versuchsbedingungen und der Ergebnisse in Form einer \emph{Mitschrift}.

\subsection{Mitschrift}
	SEHR WICHTIG\\
Ein gutes Versuchsprotokoll beginnt beim Aufzeichnen aller (wichtigen) Details eines Versuches - neben den Messergebnissen. Es ist klar, dass sie erst mit etwas Routine zwischen unwichtigen und wichtigen Details unterscheiden können. Daher ist es besser wenn sie am Beginn genauere Notizen machen. Diese dienen später als Vorlage zur Erstellung des Protokolls. \\
Beispiele für wichtige Informationen:

		\begin{itemize}
			
			\item Messgerätetyp
			\item Messmodus (z.B. Amperemeter, Voltmeter)
			\item Messbereiche oder andere spezielle Einstellungen an den Geräten
			\item Einheiten der protokollierten Messdaten (mA, µA, V, mV, µs, usw.)
			\item ...

		\end{itemize}

Beispiele für (wahrscheinlich) unwichtige Informationen:

		\begin{itemize}
			
			\item Farbe der Mess- oder Verbindungsleitungen
			\item Wetter
			\item persönliche Tagesverfassung
			\item ...

		\end{itemize}

Denken sie daran, dass auch Dinge, die während der Übung für sie sonnenklar waren, ein paar Tage später große Rätsel aufgeben können. Ebenso kann sich die Meinung \enquote{das merk ich mir eh} fatal auswirken.\\
Dazu folgende Szene:\\
Ich: \enquote{Christian wieso schaut die Diodenkennlinie so komisch aus?} Christian: \enquote{Weil du die beiden Kanäle für Strom und Spannung vertauschen musst und außerdem das Vorzeichen für den Strom verkehrt ist}. Ich: \enquote{Ahso, eh klar!} ... Tage später! ... Ich: \enquote{Komisch, was mussten wir noch schnell machen damit die Kennlinie korrekt dargestellt wird?} ... 

	\section{ Protokoll }


Ein gutes Protokoll soll die Wiederholung eines Experimentes ermöglichen. Daraus ergibt sich eine zweckdienliche, bewährte Struktur des Dokuments.

\begin{itemize}

	\item Fragestellung/ Aufgabenstellung
	\item grundlegende oder theoretische Überlegungen, vorausgehende Berechnungen, Erwartungen
	\item verwendete Geräte, Materialien, Versuchsbedingungen
	\item Versuchsaufbau, Schaltung, Einstellungen (Zeichnung, Skizze)
	\item Versuchsbeschreibung, -durchführung, -ablauf
	\item Ergebnisse, Beobachtungen, Messwerte, Fotos, Datenspeicher
	\item Auswertung, Berechnungen, Diagramme, Tabellen
	\item Schlussfolgerungen, Diskussion

\end{itemize}

\subsection{Aufgabenstellung}

Kurze Beschreibung der Messaufgabe. Dabei ist es sinnvoll auch die
verwendeten Messgeräte anzugeben. Werden besondere Anforderungen an
ein Messgerät gestellt, so ist auch z.B. die Art der Verwendung des
Messgerätes anzugeben.

\subsection{Schaltplan}

\begin{circuit}[h!]
  \centering
  \includegraphics[width=.62\textwidth]{bilder/stromrichtig}
  \caption{Beispiel zur Ausführung eines Schaltplans im
    Laborprotokoll}
  \label{cir:beispiel}
\end{circuit}

Der Schaltplan muss die Schaltung des Messaufbaus eindeutig und klar
wiedergeben. Die Schaltzeichen sind nach den gültigen Normen zu
zeichnen. Der Schaltplan ist durch die Angabe eines Titels unter dem
Schaltplan näher zu beschreiben sowie fortlaufend zu
nummerieren. Schaltplan~\ref{cir:beispiel} zeigt ein entsprechendes
Beispiel.

\subsection{Diagramme}

Bei Diagrammen ist es unerlässlich die Koordinatenachsen zu beschriften
(Maßgröße und Einheit). Von Vorteil ist auch eine klare Einteilung der Skalen
(Bezifferung). Messpunkte sollten deutlich gekennzeichnet sein ($\times$,
+, $\diamond$, $\star$ oder ähnlich). Falls mehrere Datensätze in einem Diagramm dargestellt werden müssen, sollten sich die einzelnen Graphen eindeutig unterscheiden, z.B. durch verschiedene Linienarten oder Symbole für Datenpunkte und die dazugehörige Bezeichnung. Verschiedene Farben sind oft nicht ausreichend wenn sie schwarz/weiß drucken oder kopieren. Jedes Diagramm ist durch die
Angabe eines Titels unter dem Diagramm näher zu beschreiben sowie
fortlaufend zu nummerieren. Weiters muss im Text eine Interpretation
des Diagramms vorhanden sein. Diagramm~\ref{dia:beispiel} zeigt, dass
die I-U-Kennlinie für Messanordnung A nicht ganz linear ist, und bei
kleinen Ausgangsspannungen (unter etwa 2 Volt) abflacht - warum könnte das sein?

\begin{diagram}[h!]
  \centering
  \includegraphics[width=.62\textwidth]{bilder/diagramm.png}
  \caption{Beispiel zur Ausführung eines Diagramms im Laborprotokoll}
  \label{dia:beispiel}
\end{diagram}

\subsection{Tabellen}

Tabellen sind grundsätzlich durch die jeweilige Angabe der Messgröße
und der dazugehörigen Einheit darzustellen. Die Bezeichnung der
Messgröße hat in Bezug auf die Messschaltung zu erfolgen. Die Tabelle
ist durch die Angabe eines Titels unter der Tabelle näher zu
beschreiben sowie fortlaufend zu nummerieren. Die Reihenfolge der
Größen in den Spalten richtet sich nach:
\begin{enumerate}
\item eingestellte Größen
\item gemessene Größen
\item errechnete Größen und Werte
\end{enumerate}

\begin{table}[h!]
  \centering
  \begin{tabular}{ccc}
    \toprule
    Spannung U & Strom I & Widerstand R$_x$ \\
    (V) & ($\mu$A) & (M$\Omega$) \\
    \midrule
    2,21 & 1,10 & 2,01 \\
    4,39 & 2,25 & 1,95 \\
    6,61 & 3,31 & 2,0 \\
    \midrule
     & Mittelwert & 1,99 \\
    \bottomrule
  \end{tabular}
  \caption{Beispiel zur Ausführung einer Tabelle im Laborprotokoll}
  \label{tab:beispiel}
\end{table}

\subsection{Formeln}

Die bei der Auswertung verwendeten Formeln sind im Protokoll an
entsprechender Stelle anzuführen. Einheiten für konkrete Werte sind
anzugeben.
\begin{equation*}
  I_R = \frac{U_V}{R_X} = \frac{50.2 mV}{4.7k\Omega} = 1.07 \cdot 10^{-5} A = 10.7{\mu}A
\end{equation*}

Bitte beachten sie auch, dass berechnete Werte auf signifikante Kommastellen gerundet werden 
\emph{müssen}. Es hat zum Beispiel keinen Sinn den oben berechneten Wert mit z.B. 6 Kommastellen 
anzugeben, wenn die Messwerte nur mit einer Kommastelle angezeigt werden oder die 
Toleranz des Widerstandes 1\% beträgt.

\subsection{Diskussion}

Nach jeder Messaufgabe sollte eine kurze Erläuterung über das Ergebnis
der Messung abgegeben werden, z.B. können die berechneten Werte mit
den gemessenen Werten verglichen und entstandene Abweichungen erklärt
werden. Gehen sie auch auf die, im Skriptum bei den einzelnen Versuchen angeführten,
Diskussionspunkte ein.

\section{Tipps}

Auch die Form der Laborprotokolle geht in die Benotung der Laborübung
ein. Halten sie das Protokoll klar und übersichtlich, vermeiden sie
unnötige Spielereien. Stellen sie Information so dar, dass der Inhalt
schnell erfasst werden kann (z.B. durch passende Skalierung bei
Diagrammen).  Verwenden sie eine automatische Rechtschreibüberprüfung,
um Tippfehler zu vermeiden.

\end{document}


